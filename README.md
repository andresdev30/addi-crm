# AddiCrm

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

## Instructions:

1. The start page shows both leads and prospects list. Prospects list is empty untill you move a lead after running the validations.

2. Each Lead is inside a Card with its information, to start click on the button "Validate Lead". This action starts the processes in the following order:

Step 1: Validate National Identification Registry System: Running...

Step 2: Validate Judicial Records on National Archives System: Running...

3. After this execution you will see OK or FAILED as appropiate, then it fires the third step and shows:

Step 3: Internal Qualification System: Running...

4. If everithing goes well it will show the next message and the button "Move to prospects":

Step 3: Internal Qualification System: OK

5. Click on the button "Move to prospects" and it will move the prospect to Prospects list on the right hand.

## Decisions and Assumptions

1. Inside the crm.service.ts file you will find a method called: _asynchronousSimulation()_. This method simulates a server request and accepts a parameter to set the fail thershold in order to determine if the request is success or failed.

2. There are three more methods simulating the corresponding request to the three services:

   _nationalRegistryValidation()_: Validate National Identification Registry System

   _nationalArchivesValidation()_: Validate Judicial Records on National Archives System

   _internalQualification()_: Internal Qualification System

3. I use the colors: orange, green and red to identify the results of the stages of the process.

export interface Customer {
  nid: number;
  firstName: string;
  lastName: string;
  birthDate: string;
  email: string;
  phone: string;
  step1: string;
  step2: string;
  step3: string;
  canBeProspect?: boolean;
}

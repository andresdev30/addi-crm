import { Component, Input, OnInit } from '@angular/core';
import { Customer } from 'src/app/models/customer.model';

@Component({
  selector: 'app-prospects',
  templateUrl: './prospects.component.html',
  styleUrls: ['./prospects.component.scss'],
})
export class ProspectsComponent implements OnInit {
  @Input() prospects: Customer[] = [];

  constructor() {}

  ngOnInit(): void {}
}

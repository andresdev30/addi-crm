import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { Customer } from 'src/app/models/customer.model';
import { CrmService } from 'src/app/services/crm.service';

@Component({
  selector: 'app-leads',
  templateUrl: './leads.component.html',
  styleUrls: ['./leads.component.scss'],
})
export class LeadsComponent implements OnInit {
  @Input() leads: Customer[] = [];
  @Output()
  sendToProspects: EventEmitter<Customer> = new EventEmitter<Customer>();

  constructor(private crmService: CrmService) {}

  ngOnInit(): void {}

  async onSelect(lead: Customer): Promise<void> {
    lead.step1 =
      'Step 1: Validate National Identification Registry System: Running...';
    lead.step2 =
      'Step 2: Validate Judicial Records on National Archives System: Running...';
    lead.step3 = '';
    lead.canBeProspect = false;
    await Promise.all([
      this.crmService.nationalRegistryValidation(lead),
      this.crmService.nationalArchivesValidation(lead),
    ]);
    lead.step1 = 'Step 1: Validate National Identification Registry System: OK';
    lead.step2 =
      'Step 2: Validate Judicial Records on National Archives System: OK';
    lead.step3 = 'Step 3: Internal Qualification System: Running...';
    this.crmService.internalQualification(lead).subscribe((res) => {
      if (res) {
        lead.step3 = 'Step 3: Internal Qualification System: OK';
        lead.canBeProspect = true;
      } else {
        lead.step3 =
          'Step 3: Internal Qualification System: FAILED, the lead cannot be prospect';
      }
    });
  }

  onMoveToProspects(lead: Customer): void {
    this.sendToProspects.emit(lead);
  }
}

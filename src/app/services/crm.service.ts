import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Customer } from '../models/customer.model';

@Injectable({
  providedIn: 'root',
})
export class CrmService {
  constructor() {}

  nationalRegistryValidation(lead: Customer): Promise<boolean> {
    return this.asynchronousSimulation(50);
  }

  nationalArchivesValidation(lead: Customer): Promise<boolean> {
    return this.asynchronousSimulation(40);
  }

  internalQualification(lead: Customer): Observable<boolean> {
    const RANDOM_TIME = Math.floor(Math.random() * 4000 + 1000);
    return new Observable((observer) => {
      setTimeout(() => {
        const result = Math.floor(Math.random() * 100 + 1) > 50 ? true : false;
        observer.next(result);
      }, RANDOM_TIME);
    });
  }

  asynchronousSimulation(failThreshold: number): Promise<boolean> {
    const RANDOM_TIME = Math.floor(Math.random() * 4000 + 1000);
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        const response =
          Math.floor(Math.random() * 100 + 1) > failThreshold ? true : false;
        resolve(response);
        reject(new Error('Message'));
      }, RANDOM_TIME);
    });
  }
}

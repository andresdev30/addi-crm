import { TestBed } from '@angular/core/testing';

import { CrmService } from './crm.service';
import { Customer } from '../models/customer.model';

describe('CrmService', () => {
  let service: CrmService;
  const lead: Customer = {
    nid: 111111111,
    firstName: 'Andrés',
    lastName: 'Estepa',
    birthDate: '04/24/1987',
    email: 'cael19@gmail.com',
    phone: '3176992551',
    step1: '',
    step2: '',
    step3: '',
  };

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CrmService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('returns boolean simulating a server request based on a fail threshold', () => {
    (done: DoneFn) => {
      service.asynchronousSimulation(10).then((value) => {
        expect(value).toBe(true, false);
        done();
      });
    };
  });

  it('returns boolean', (done: DoneFn) => {
    service.internalQualification(lead).subscribe((value) => {
      expect(value).toBe(true, false);
      done();
    });
  });
});

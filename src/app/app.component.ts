import { Component } from '@angular/core';
import { Customer } from './models/customer.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  leads: Customer[] = [
    {
      nid: 111111111,
      firstName: 'Andrés',
      lastName: 'Estepa',
      birthDate: '04/24/1987',
      email: 'cael19@gmail.com',
      phone: '3176992551',
      step1: '',
      step2: '',
      step3: '',
    },
    {
      nid: 222222222,
      firstName: 'Carlos',
      lastName: 'Lozano',
      birthDate: '04/24/1985',
      email: 'cael19@gmail.com',
      phone: '3176992551',
      step1: '',
      step2: '',
      step3: '',
    },
    {
      nid: 333333333,
      firstName: 'Maria',
      lastName: 'Lopez',
      birthDate: '11/04/1990',
      email: 'maria@gmail.com',
      phone: '3176992551',
      step1: '',
      step2: '',
      step3: '',
    },
  ];

  prospects: Customer[] = [];

  addProspect(lead: Customer): void {
    this.removeLead(lead);
    this.prospects.push(lead);
  }

  removeLead(lead: Customer): void {
    this.leads = this.leads.filter((l) => l.nid !== lead.nid);
  }
}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { LeadsComponent } from './components/leads/leads.component';
import { ProspectsComponent } from './components/prospects/prospects.component';

@NgModule({
  declarations: [AppComponent, LeadsComponent, ProspectsComponent],
  imports: [BrowserModule, RouterModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
